function pause_play() {
    for (var e = 1; e < 50; e++) {
        var n = document.getElementById("numSound" + e);
        n.play(), n.pause(), 1 == e ? Engnum = "one" : 2 == e ? Engnum = "two" : 3 == e ? Engnum = "three" : 4 == e ? Engnum = "four" : 5 == e ? Engnum = "five" : 6 == e ? Engnum = "six" : 7 == e && (Engnum = "seven"), (n = document.getElementById("numSound_" + Engnum)).play(), n.pause()
    }
}

function orplay() {
    return $("#die").hasClass("play") ? (console.log(!0), !0) : (console.log(!1), !1)
}

function createArr() {
    for (var e = [], n = 0; n < 7; n++) {
        var o = createNum(1, 49);
        if (0 != n)
            for (var t = 0, a = e.length - 1; t < e.length; t++) {
                if (o == e[t]) {
                    n--;
                    break
                }
                if (t == a) {
                    e.push(o);
                    break
                }
            } else e.push(o)
    }
    return e
}

function createNum(e, n) {
    var o = Math.random() * (n - e),
        t = Math.round(o + e);
    return t = Math.max(Math.min(t, n), e)
}

function addDOM(e, n) {
    if (clearInterval(a), 0 == n) var o = "",
        t = 0,
        a = setInterval(function() {
            if (stopsound) return clearInterval(a), clearTimeout(u), void(numSound = document.getElementById("numSound"));
            var n = e[t] > 9 ? e[t] : "0" + e[t];
            shakeBall(BooColor[e[t]], t), o = 6 != t ? "<li class='" + BooColor[e[t]] + "'>" + n + "</li>" : "<li class='" + BooColor[e[t]] + " lastLi'>" + n + "</li>", panduSound(t, e[t], e.length);
            var l = setTimeout(function() {
                $(".number>ul").append(o), clearTimeout(l)
            }, 2e3);
            if (7 == ++t) {
                clearInterval(a);
                var u = setTimeout(function() {
                    stopanimate(), clearTimeout(u)
                }, 5e3)
            }
        }, 4e3);
    else {
        o = "";
        if (breckClond = e.length, void 0 == oldI) t = 0;
        else t = oldI + 1;
        if (t < oldI) return !1;
        if (NaN == t) t = 0;
        console.log(t);
        a = setInterval(function() {
            if (console.log("oldI " + oldI, "   i " + t), void 0 != e[t] && 0 != e[t]) {
                if (oldI != t) {
                    u = e[t] > 9 ? e[t] : "0" + e[t];
                    shakeBall(BooColor[e[t]], t), o = 6 != t ? "<li class='" + BooColor[e[t]] + "'>" + u + "</li>" : "<li class='" + BooColor[e[t]] + " lastLi'>" + u + "</li>", panduSound(t, e[t], breckClond), oldI = t;
                    var n = setTimeout(function() {
                        $(".number>ul").append(o), 7 == t && (originalHtml = $(".number>ul").html()), clearTimeout(n)
                    }, 2e3);
                    if (7 == ++t) {
                        clearInterval(a);
                        var l = setTimeout(function() {
                            nextopenTime(nextopent), stopanimate(), clearTimeout(l)
                        }, 5e3)
                    }
                } else breckClond > t && t++;
                var u = e[t] > 9 ? e[t] : "0" + e[t]
            } else t = oldI + 1
        }, 4e3)
    }
}

function shakeBall(e, n) {
    var o = n + 1,
        t = "16px",
        a = "114px",
        l = "14px 108px";
    $(".min").width() <= 983 && (t = "0.2rem", a = "1.45rem", l = "0.2rem 1.35rem");
    $(".ball>span");
    $(".ball>span:nth-child(" + o + ")").show().addClass(e).animate({
        top: t,
        left: a
    }, 500);
    var u = "";
    1 == o ? u = -168 : 2 == o ? u = -160 : 3 == o ? u = -152 : 4 == o ? u = -145 : 5 == o ? u = -138 : 6 == o ? u = -130 : 7 == o && (u = 173);
    var s = setTimeout(function() {
        7 != o ? $(".ball>span:nth-child(" + o + ")").addClass("rotate").css("transform", "rotate(" + u + "deg)") : $(".ball>span:nth-child(" + o + ")").addClass("rotate").css({
            "transform-origin": l,
            transform: "rotate(" + u + "deg)"
        }), clearTimeout(s)
    }, 500)
}

function stopanimate() {
    if (stopsound = !stopsound, clearInterval(addinter), $(".min").width() <= 983) var e = "2.6rem",
        n = "1.45rem";
    else var e = "199px",
        n = "114px";
    $(".ball>span").removeClass(), $("#die").removeClass("play"), $("#die").css("cssText", "background-image:url(http://kj.kai861.com/view/video/SixColor_animate/img/0001.png) !important"), $(".status").css("display", "initial").siblings(".each").css("display", "initial"), $(".status").css("background-image", "url(http://kj.kai861.com/view/video/SixColor_animate/img/0001.png)"), $(".ball>span").removeClass("rotate").css({
        transform: "rotate(0deg)",
        top: e,
        left: n,
        display: "none"
    }), bgSound.pause(), numSound.pause(), numSound = document.getElementById("numSound"), $(".number>ul").html(originalHtml)
}

function panduSound(e, n, o) {
    function t(e) {
        var n = setTimeout(function() {
            clearTimeout(n), numSound = document.getElementById("numSound" + e), $("#sound").hasClass("on") && numSound.play()
        }, 2e3)
    }
    switch (e) {
        case 0:
            numSound = document.getElementById("numSound_one"), $("#sound").hasClass("on") && numSound.play(), t(n);
            break;
        case 1:
            numSound = document.getElementById("numSound_two"), $("#sound").hasClass("on") && numSound.play(), t(n);
            break;
        case 2:
            numSound = document.getElementById("numSound_three"), $("#sound").hasClass("on") && numSound.play(), t(n);
            break;
        case 3:
            numSound = document.getElementById("numSound_four"), $("#sound").hasClass("on") && numSound.play(), t(n);
            break;
        case 4:
            numSound = document.getElementById("numSound_five"), $("#sound").hasClass("on") && numSound.play(), t(n);
            break;
        case 5:
            numSound = document.getElementById("numSound_six"), $("#sound").hasClass("on") && numSound.play(), t(n);
            break;
        case 6:
            numSound = document.getElementById("numSound_seven"), $("#sound").hasClass("on") && numSound.play(), t(n)
    }
    console.log("第幾個號碼 " + e, "arr長度 " + o), 7 != e && e + 2 >= o && 7 != o && (console.log("timeout"), typetwo = setTimeout(function() {
        clearTimeout(typetwo), loadAjax()
    }, 2e3))
}

function loadAjax() {
    $.ajax({
        type: "get",
        url: "get_Lotterydata_xgc",
        async: !0,
        dataType: "json",
        success: function(e) {
            if (console.log(e), nextopent = e.nextdate, dateAndissue(e), 4 == e.type) clearInterval(addinter), stopanimate(), addloadhtml(e), nextopenTime(e.nextdate);
            else if (6 == e.type) $("#nextTime").text("請不要走開，今天晚上21:30開獎..."), bgSound.play(), typeSix = setTimeout(function() {
                clearTimeout(typeSix), loadAjax()
            }, 6e4);
            else if (0 == e.type) $("#nextTime").text("準備報碼，請稍後..."), typeten = setTimeout(function() {
                clearTimeout(typeten), loadAjax()
            }, 1e4);
            else if (2 == e.type) $("#nextTime").text("節目廣告中..."), typeten = setTimeout(function() {
                clearTimeout(typeten), loadAjax()
            }, 1e4);
            else if (3 == e.type) $("#nextTime").text("主持人解說中..."), typeten = setTimeout(function() {
                clearTimeout(typeten), loadAjax()
            }, 1e4);
            else if (1 == e.type) {
                $("#nextTime").text("開獎中...");
                for (var n = e.ma.split(","), o = [], t = 0; t < n.length; t += 3) o.push(1 * n[t]);
                if (breckClond != o.length) kaijiIn(o);
                else var a = setTimeout(function() {
                    console.log("跟上一期結果相同，請重新整理"), loadAjax(), clearTimeout(a)
                }, 2e3)
            }
        },
        error: function(e) {
            console.log("error+++++++" + e), errcount <= 5 && loadAjax(), errcount++
        }
    })
}

function kaijiIn(e) {
    if ("" == In) {
        $("#sound").hasClass("on") && (bgSound.play(), numSound.play()), $("#die").addClass("play"), $(".status").css("background-image", "url(http://kj.kai861.com/view/video/SixColor_animate/img/status.gif)");
        var n = setTimeout(function() {
                $(".each").css("display", "none"), $("#die").css("background-image", "initial"), clearTimeout(n)
            }, 1e3),
            o = setTimeout(function() {
                $(".each").css("display", "block").css("background-image", "url(http://kj.kai861.com/view/video/SixColor_animate/img/each.gif)"), addDOM(e, !0), clearTimeout(o)
            }, 2500);
        In = !0
    } else addDOM(e, !0)
}

function addloadhtml(e) {
     //json_data2 = JSON.stringify(e);
     //alert(json_data2);
    console.log(e);
    for (var n = e.ma.split(","), o = [], t = 0; t < n.length; t += 3) o.push(1 * n[t]);
    var a = "",
        l = "";
    $.each(o, function(e, n) {
        a = n > 9 ? n : "0" + n, l += 6 != e ? "<li class='" + BooColor[n] + "'>" + a + "</li>" : "<li class='" + BooColor[n] + " lastLi'>" + a + "</li>"
    }), $(".number>ul").html(l), originalHtml = l
}

function dateAndissue(e) {
    var n = e.nextdate.slice(0, 10).replace("/", "年").replace("/", "月") + "號";
    $("#data_b").text(n), $("#issue_b").text(e.nextid)
}

function nextopenTime(e) {
    console.log(new Date(e));
    setInterval(function() {
        var n = new Date(e).getTime() - (new Date).getTime(),
            o = parseInt(n / 36e5 / 24),
            t = parseInt(n / 36e5 - 24 * o),
            a = parseInt(60 * (n / 36e5 - 24 * o - t)),
            l = parseInt(60 * (60 * (n / 36e5 - 24 * o - t) - a));
        if (!(n < 0 || o < 0 || n < 0 || n < 0 || n < 0)) {
            var u = "距離下期開獎還有" + o + "天" + t + "小時" + a + "分" + l + "秒";
            $("#nextTime").text(u)
        }
    }, 1e3)
}
window.onload = function() {
    $(".loading").hide()
};
var oldLog = console.log;
console.log = function() {}, loadAjax();
var BooColor = ["", "red", "red", "blue", "blue", "green", "green", "red", "red", "blue", "blue", "green", "red", "red", "blue", "blue", "green", "green", "red", "red", "blue", "green", "green", "red", "red", "blue", "blue", "green", "green", "red", "red", "blue", "green", "green", "red", "red", "blue", "blue", "green", "green", "red", "blue", "blue", "green", "green", "red", "red", "blue", "blue", "green"],
    bgSound = document.getElementById("bgSound"),
    numSound = document.getElementById("numSound"),
    originalHtml = "",
    stopsound = !1;
$("#sound").click(function(e) {
    e.preventDefault(), $(this).hasClass("on") ? ($(this).removeClass("on").addClass("off"), bgSound.pause(), numSound.pause()) : ($(this).removeClass("off").addClass("on"), orplay() && (bgSound.play(), numSound.play()))
}), $("#try").click(function(e) {
    if (e.preventDefault(), stopsound = !1, !orplay()) {
        originalHtml = $(".number>ul").html(), $(".number>ul").html(""), $("#sound").hasClass("on") && (bgSound.play(), numSound.play()), $("#die").addClass("play"), $(".status").css("background-image", "url(http://kj.kai861.com/view/video/SixColor_animate/img/status.gif)");
        var n = setTimeout(function() {
                $(".each").css("display", "none"), $("#die").css("cssText", "background-image:url(http://kj.kai861.com/view/video/SixColor_animate/img/empt.png) !important"), clearTimeout(n)
            }, 1e3),
            o = setTimeout(function() {
                $(".each").css("display", "block").css("background-image", "url(http://kj.kai861.com/view/video/SixColor_animate/img/each.gif)"), addDOM(createArr(), !1), clearTimeout(o)
            }, 2500)
    }
});
var addinter, oldI, breckClond, typeSix, typeten, typetwo, nextopent, errcount = 0,
    In = "";