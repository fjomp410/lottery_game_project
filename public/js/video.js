var pubmethod = {},
    path = window.location.href,
    lotCode;
$(function() {
    pubmethod.init(), $("#zixunHref").on("click", function() {
        $(this).attr("href", "index.html" + config.ym())
    }), $("#touzhuHref").on("click", function() {
        $(this).attr("href", "touzhu_index.html" + config.ym())
    }), $("#videoHref").on("click", function() {
        $(this).attr("href", "video_index.html" + config.ym())
    }), $("#kaiholeHref").on("click", function() {
        $(this).attr("href", "kaihole_index.html" + config.ym())
    })
}), pubmethod.init = function() {
    if (-1 != path.indexOf("video_index") || void 0 != path.split("?")[1] && "" != path.split("?")[1]) {
        lotCode = path.split("?")[1];
        var e = pubmethod.tools.type(lotCode);
        void 0 != e && pubmethod.doAjax("", lotCode, e, !0)
    } else alert("Start")
}, pubmethod.tools = {
    type: function(e) {
        for (var t = [
                ["cqnc", "10009"],//幸運農場
                ["xgc", "10048", "10051"],//香港六合彩
                ["egxy", "10046"],//PC蛋蛋
                ["gxklsf", "10038"],//廣西快樂十分
                ["jisuft", "10035", "10057", "10058"],//急速快艇
                ["twbg", "10047"],
                ["fcsd", "10041", "10043"],//排列3
                ["bjkl8", "10013", "10014", "10054"],//極速快樂8
                ["klsf", "10005", "10011", "10034", "10053"],//廣東快樂十分
                ["pk10", "10001", "10012", "10037"],//賽車
                ["qgc", "10039", "10040", "10042", "10044", "10045"],
                ["ssc", "10002", "10003", "10004", "10010", "10036", "10050", "10059", "10060"],//天津財財彩
                ["kuai3", "10007", "10026", "10027", "10028", "10029", "10030", "10031", "10032", "10033", "10052"],//江蘇快3
                ["shiyi5", "10006", "10008", "10015", "10016", "10017", "10018", "10019", "10020", "10021", "10022", "10023", "10024", "10025", "10055"],//廣東11選5
                //本地測試頻道
                ["Abao_pk10","00000"],
                ['Abao_jisuft',"00005"],
                ["Abao_ssc","00001"],
                ['Abao_klsf','00002'],
                ['Abao_shiyi5','00003'],
                ['Abao_kuai3','00004'],
                ['Abao_bjkl8','00006'],
                ['Abao_cqnc','00007'],
                ['Abao_gxklsf','00008'],
                ['Abao_fcsd','00009'],
                ['Abao_egxy','00010'],

            ], o = 0, r = t.length; o < r; o++)
            for (var s = 0, i = t[o].length; s < i; s++)
                if (e == t[o][s]) return t[o][0]
    },
    action: {
        pk10: "pks/getLotteryPksInfo.do",
        cqnc: "klsf/getLotteryInfo.do",
        ssc: "CQShiCai/getBaseCQShiCai.do",
        klsf: "klsf/getLotteryInfo.do",
        jsk3: "lotteryJSFastThree/getBaseJSFastThree.do",
        shiyi5: "ElevenFive/getElevenFiveInfo.do",
        bjkl8: "LuckTwenty/getBaseLuckTewnty.do",
        twbg: "LuckTwenty/getBaseLuckTewnty.do",
        egxy: "LuckTwenty/getPcLucky28.do",
        gxklsf: "gxklsf/getLotteryInfo.do",
        kuai3: "lotteryJSFastThree/getBaseJSFastThree.do",
        fcsd: "QuanGuoCai/getLotteryInfo1.do",
        jisuft: "pks/getLotteryPksInfo.do",
        //本地測試頻道
        Abao_pk10: 'get_Lotterydata_pk10',
        Abao_jisuft:'get_Lotterydata_jisuft',
        Abao_ssc: "get_Lotterydata_ssc",
        Abao_klsf:'get_Lotterydata_klsf',
        Abao_shiyi5:'get_Lotterydata_shiyi5',
        Abao_kuai3:'get_Lotterydata_kuai3',
        Abao_bjkl8:'get_Lotterydata_bjkl8',
        Abao_cqnc:'get_Lotterydata_cqnc',
        Abao_gxklsf:'get_Lotterydata_gxklsf',
        Abao_fcsd: 'get_Lotterydata_fc3d',
        Abao_egxy:'get_Lotterydata_egxy',
    },
    pageView: function(e) {
        return {
            cqnc: "video/cqnc/index.html",
            egxy: "video/pcEgg_video/index.html",
            gxklsf: "video/gxklsf_video/index.html",
            fcsd: "video/fc3DVideo/index.html",
            bjkl8: "video/bjkl8Video/index.html",
            twbg: "video/twbgVideo/twbg_index.html",
            klsf: "video/GDklsf/index.html",
            pk10: "video/PK10/video.html",
            qgc: "video/PK10/video.html",
            ssc: "video/SSC/index.html",
            kuai3: "video/kuai3_video/Kuai3.html",
            shiyi5: "video/11x5_video/index.html",
            jisuft: "video/jisuft_video/index.html",
            xgc: "video/SixColor_animate/index.html",
            Abao_pk10: "video/PK99/video.html",
            Abao_jisuft:"video/jisuft_video/index.html",
            Abao_ssc: "video/SSC/index.html",
            Abao_klsf: "video/GDklsf/index.html",
            Abao_shiyi5: "video/11x5_video/index.html",
            Abao_bjkl8:'video/bjkl8Video/index.html',
            Abao_cqnc:'video/cqnc/index.html',
            Abao_gxklsf:'video/gxklsf_video/index.html',
            Abao_fcsd:'video/fc3DVideo/index.html',
            Abao_egxy:'video/pcEgg_video/index.html',
        } [e]
    },
    random: function() {
        return (new Date).getTime()
    },
    //製作e參數
    ifObj: function(e) {
        var t = null;
        return "object" != typeof e ? t = JSON.parse(e) : (t = JSON.stringify(e), t = JSON.parse(t)), t
    },
    cutTime: function(e, t) {
        var o = e.replace("-", "/"),
            t = t.replace("-", "/");
        return o = o.replace("-", "/"), t = t.replace("-", "/"), (new Date(o) - new Date(t)) / 1e3
    }
}, pubmethod.repeatAjax = function(e, t) {
 
    setTimeout(function() {
        e(t)
    }, config.startTime)
    //製作t參數
}, pubmethod.doAjax = function(e, t, o, r) {

    var s = {
        url: pubmethod.tools.action[o],
        issue: e,
        lotCode: t,
        flag: r,
        type: o,
        succM: function(e, t) {

            pubmethod.creatHeadD[o](e, t)
        }
    };
    pubmethod.ajaxM(s)

}, pubmethod.ajaxM = function(e) {

    $.ajax({
        url: config.publicUrl + "" + e.url,
        type: "GET",
        dataType:"JSON",
        async: !0,
        data: {
            issue: void 0 == e.issue ? "" : e.issue,
            lotCode: e.lotCode,
            datestr: ""
        },
        timeout: "6000",
        success: function(t) {
            
            try {
                e.succM(t, e)
            } catch (t) {
                pubmethod.repeatAjax(pubmethod.ajaxM, e)
            }
        },
        error: function(t) {
            pubmethod.repeatAjax(pubmethod.ajaxM, e)
        }
    })
}, pubmethod.creatHeadD = {
    Abao_pk10: function(e, t) {
        //json_data = JSON.stringify(e);
        //alert(json_data);
        var o = pubmethod.tools.ifObj(e);

        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            //var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime)
            //下期開始時間drawTime、當前時間serverTime
            //alert(o.drawTime);
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = "", u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i += s[u].substr(1, 1) + "," : i += s[u] + ",";
            //r倒數剩餘時間(等於0才會開始執行)
            if (r = r < 0 ? 1 : r, showcurrentresult(i), $("#currentdrawid").text(o.drawCount), $("#nextdrawid").text(o.preDrawIssue), $("#stat1_1").text(o.sumFS), $("#stat1_2").text("0" == o.sumBigSamll ? "大" : "小"), $("#stat1_3").text("0" == o.sumSingleDouble ? "單" : "雙"), $("#stat2_1").text("0" == o.firstDT ? "龍" : "虎"), $("#stat2_2").text("0" == o.secondDT ? "龍" : "虎"), $("#stat2_3").text("0" == o.thirdDT ? "龍" : "虎"), $("#stat2_4").text("0" == o.fourthDT ? "龍" : "虎"), $("#stat2_5").text("0" == o.fifthDT ? "龍" : "虎"), t.flag) $("#hlogo").find("img").attr("src", "http://kj.kai861.com/view/video/PK10/images/logo/logo-" + '10012' + ".png"), $(".statuslogo").css({
                background: "url(http://kj.kai861.com/view/video/PK10/images/logo/logo-" + "10012" + ".png)no-repeat"
            }), startcountdown(r, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    finishgame(i)
                }, "1000"), setTimeout(function() {
                    startcountdown(r - 11, t)
                }, "10000")
            }
        }
    },
    Abao_jisuft: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data, $("#status").css("background-image", "http://kj.kai861.com/view/video/jisuft_video/images/logo_10035.png"), $(".logo").find("img").attr("src", "images/logo_" + o.lotCode + ".png");
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            if (r = r < 0 ? 1 : r, o.cutime = r, console.log(o), showcurrentresult(o.preDrawCode), $("#currentdrawid").text(o.drawCount), $("#nextdrawid").text(o.preDrawIssue), $("#stat1_1").text(o.sumFS), $("#stat1_2").text("0" == o.sumBigSamll ? "大" : "小"), $("#stat1_3").text("0" == o.sumSingleDouble ? "單" : "雙"), $("#stat2_1").text("0" == o.firstDT ? "龍" : "虎"), $("#stat2_2").text("0" == o.secondDT ? "籠" : "虎"), $("#stat2_3").text("0" == o.thirdDT ? "龍" : "虎"), $("#stat2_4").text("0" == o.fourthDT ? "龍" : "虎"), $("#stat2_5").text("0" == o.fifthDT ? "龍" : "虎"), t.flag) startcountdown(r, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    finishgame(i.toString())
                }, "1000"), setTimeout(function() {
                    startcountdown(r - 11, t)
                }, "10000")
            }
        }
    },
    Abao_ssc: function(e, t) {
        //json_data = JSON.stringify(t);
        //alert(json_data);
        if ("100002" == (a = pubmethod.tools.ifObj(e)).result.businessCode) throw new Error("error");
        if (0 == a.errorCode && 0 == a.result.businessCode) {
            for (var o = (a = a.result.data).lotCode, r = pubmethod.tools.cutTime(a.drawTime, a.serverTime), s = a.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) && s[u].length > 1 ? i.push(s[u].substr(1, 1)) : i.push(s[u]);
            r = r < 0 ? 1 : r;
            var l = "";
            "0" == a.dragonTiger ? l = "龍" : "1" == a.dragonTiger ? l = "虎" : "2" == a.dragonTiger && (l = "和");
            var a = {
                preDrawCode: i,
                id: "#numBig",
                counttime: r,
                preDrawIssue: a.preDrawIssue,
                drawTime: a.drawTime.substr(a.drawTime.length - 8, 8),
                sumNum: a.sumNum,
                sumSingleDouble: 0 == a.sumSingleDouble ? "單" : "雙",
                sumBigSmall: 0 == a.sumBigSmall ? "大" : "小",
                dragonTiger: l
            };
            if (t.flag) sscAnimateEnd(a, t), $("#hlogo").find("img").attr("src", "http://kj.kai861.com/view/video/SSC/img/cqssc/logo-10002.png");
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    sscAnimateEnd(a, t)
                }, "1000")
            }
            "10002" != o && "10050" != lotCode || new Date("2019-03-29 23:52:00").getTime() - (new Date).getTime() <= 0 && $(".djs").html("<span  style='text-align:center;width:100%;color: #ff0b0b;display:inline-block;font-size:17px;'>停止銷售</span>")
        }
    },
    Abao_klsf: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            r = r < 0 ? 1 : r;
            var l = o.preDrawIssue,
                a = o.drawIssue,
                d = o.drawTime.split(" ")[1].slice(0, 5);
            if (t.flag) $(".video_box").css("background", "http://kj.kai861.com/view/video/GDklsf/img/logo/10005.jpg"), fun.fillHtml(l, a, d, r, i, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    fun.Trueresult(i), fun.fillHtml(l, a, d, r, void 0, t)
                }, "1000")
            }
        }
    },
    Abao_shiyi5: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if (console.log(o), "100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            if (r = r < 0 ? 1 : r, console.log(i), t.flag) $(".nameLogo").find("img").attr("src", "http://kj.kai861.com/view/video/11x5_video/img/logo/11x5_10008.png"), k3v.startVideo(o, t), console.log($(".nameLogo"), t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                console.log(o), setTimeout(function() {
                    k3v.stopVideo(o, t)
                }, "1000")
            }
        }
    },
    Abao_kuai3: function(e, t) {
        //json_data = JSON.stringify(e);
        //alert(json_data);
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            r = r < 0 ? 1 : r;
            o.drawTime.split(" ")[1].slice(0, 5);
            console.log(o);
            var l = {
                seconds: r,
                preDrawCode: i,
                sumNum: o.sumNum,
                drawTime: o.drawTime,
                drawIssue: o.drawIssue,
                preDrawIssue: o.preDrawIssue
            };
            if (t.flag) $(".nameLogo").find("img").attr("src", "http://kj.kai861.com/view/video/kuai3_video/img/logo/10007.png"), k3v.stopVideo(l, t);

            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    k3v.stopVideo(l, t)
                }, "1000")
            }
        }
    },
    Abao_bjkl8: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            if (r = r < 0 ? 1 : r, o.cutime = r, console.log(o), o.preDrawCode = i, t.flag) $(".logo").css("background", "url(http://kj.kai861.com/view/video/bjkl8Video/img/azxy20.png) center center no-repeat"), syxwV.startVid(o, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    syxwV.stopVid(o, t)
                }, "1000")
            }
        }
    },
    Abao_cqnc: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(s[u].substr(1, 1)) : i.push(s[u]);
            if (r = r < 0 ? 1 : r, t.flag) cqncVideo.statusFun(o.preDrawIssue, i, r, !0, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    stopanimate(i, r, t)
                }, "1000")
            }
        }
    },
    Abao_gxklsf: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            if (r = r < 0 ? 1 : r, o.cutime = r, console.log(o), o.numArr = i, t.flag) gxklsf.startVid(o, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    gxklsf.stopVid(o, t)
                }, "1000")
            }
        }
    },
    Abao_fcsd: function(e, t) {
        
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            r = r < 0 ? 1 : r, o.cutime = r;
            o.drawTime.split(" ")[1];
            if (console.log(o), o.preDrawCode = i, t.flag) $(".logo").css("background", "url(http://kj.kai861.com/view/video/fc3DVideo/img/logo/10043.png) center center no-repeat"), fcsdv.startVid(o, t);
            else {
                if (t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    fcsdv.stopVid(o, t)
                }, "1000")
            }
        }
    },
    Abao_egxy: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data, console.log(o);
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(s[u].substr(1, 1)) : i.push(s[u]);
            r = r < 0 ? 1 : r;
            var l = {
                nextIssue: o.drawIssue,
                drawTime: o.drawTime,
                serverTime: o.serverTime,
                numArr: i,
                preDrawTime: o.preDrawTime
            };
            if (t.flag) pcEgg.startVid(l, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    pcEgg.stopVid(l, t)
                }, "1000")
            }
        }
    },
    pk10: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            //var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime)
            //下期開始時間drawTime、當前時間serverTime
            //alert(o.drawTime);
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = "", u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i += s[u].substr(1, 1) + "," : i += s[u] + ",";
            //r倒數剩餘時間(等於0才會開始執行)
            if (r = r < 0 ? 1 : r, showcurrentresult(i), $("#currentdrawid").text(o.drawCount), $("#nextdrawid").text(o.preDrawIssue), $("#stat1_1").text(o.sumFS), $("#stat1_2").text("0" == o.sumBigSamll ? "憭�" : "撠�"), $("#stat1_3").text("0" == o.sumSingleDouble ? "���" : "���"), $("#stat2_1").text("0" == o.firstDT ? "樴�" : "���"), $("#stat2_2").text("0" == o.secondDT ? "樴�" : "���"), $("#stat2_3").text("0" == o.thirdDT ? "樴�" : "���"), $("#stat2_4").text("0" == o.fourthDT ? "樴�" : "���"), $("#stat2_5").text("0" == o.fifthDT ? "樴�" : "���"), t.flag) $("#hlogo").find("img").attr("src", "http://kj.kai861.com/view/video/PK10/images/logo/logo-" + t.lotCode + ".png"), $(".statuslogo").css({
                background: "url(http://kj.kai861.com/view/video/PK10/images/logo/logo-" + t.lotCode + ".png)no-repeat"
            }), startcountdown(r, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    finishgame(i)
                }, "1000"), setTimeout(function() {
                    startcountdown(r - 11, t)
                }, "10000")
            }
        }
    },
    egxy: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data, console.log(o);
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(s[u].substr(1, 1)) : i.push(s[u]);
            r = r < 0 ? 1 : r;
            var l = {
                nextIssue: o.drawIssue,
                drawTime: o.drawTime,
                serverTime: o.serverTime,
                numArr: i,
                preDrawTime: o.preDrawTime
            };
            if (t.flag) pcEgg.startVid(l, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    pcEgg.stopVid(l, t)
                }, "1000")
            }
        }
    },
    cqnc: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(s[u].substr(1, 1)) : i.push(s[u]);
            if (r = r < 0 ? 1 : r, t.flag) cqncVideo.statusFun(o.preDrawIssue, i, r, !0, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    stopanimate(i, r, t)
                }, "1000")
            }
        }
    },
    ssc: function(e, t) {
        
        if ("100002" == (a = pubmethod.tools.ifObj(e)).result.businessCode) throw new Error("error");
        if (0 == a.errorCode && 0 == a.result.businessCode) {
            for (var o = (a = a.result.data).lotCode, r = pubmethod.tools.cutTime(a.drawTime, a.serverTime), s = a.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) && s[u].length > 1 ? i.push(s[u].substr(1, 1)) : i.push(s[u]);
            r = r < 0 ? 1 : r;
            var l = "";
            "0" == a.dragonTiger ? l = "樴�" : "1" == a.dragonTiger ? l = "���" : "2" == a.dragonTiger && (l = "���");
            var a = {
                preDrawCode: i,
                id: "#numBig",
                counttime: r,
                preDrawIssue: a.preDrawIssue,
                drawTime: a.drawTime.substr(a.drawTime.length - 8, 8),
                sumNum: a.sumNum,
                sumSingleDouble: 0 == a.sumSingleDouble ? "���" : "���",
                sumBigSmall: 0 == a.sumBigSmall ? "憭�" : "撠�",
                dragonTiger: l
            };
            if (t.flag) sscAnimateEnd(a, t), $("#hlogo").find("img").attr("src", "http://kj.kai861.com/view/video/SSC/img/cqssc/logo-" + t.lotCode + ".png");
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    sscAnimateEnd(a, t)
                }, "1000")
            }
            "10002" != o && "10050" != lotCode || new Date("2019-03-29 23:52:00").getTime() - (new Date).getTime() <= 0 && $(".djs").html("<span  style='text-align:center;width:100%;color: #ff0b0b;display:inline-block;font-size:17px;'>�𨀣迫���睸</span>")
        }
    },
    shiyi5: function(e, t) {
        //json_data = JSON.stringify(e);
        //alert(json_data);
        var o = pubmethod.tools.ifObj(e);
        if (console.log(o), "100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            if (r = r < 0 ? 1 : r, console.log(i), t.flag) $(".nameLogo").find("img").attr("src", "http://kj.kai861.com/view/video/11x5_video/img/logo/11x5_10008.png"), k3v.startVideo(o, t), console.log($(".nameLogo"), t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                console.log(o), setTimeout(function() {
                    k3v.stopVideo(o, t)
                }, "1000")
            }
        }
    },
    klsf: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            r = r < 0 ? 1 : r;
            var l = o.preDrawIssue,
                a = o.drawIssue,
                d = o.drawTime.split(" ")[1].slice(0, 5);
            if (t.flag) $(".video_box").css("background", "url(http://kj.kai861.com/view/video/GDklsf/img/logo/10005.jpg) 0 0 no-repeat"), fun.fillHtml(l, a, d, r, i, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    fun.Trueresult(i), fun.fillHtml(l, a, d, r, void 0, t)
                }, "1000")
            }
        }
    },
    kuai3: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            r = r < 0 ? 1 : r;
            o.drawTime.split(" ")[1].slice(0, 5);
            console.log(o);
            var l = {
                seconds: r,
                preDrawCode: i,
                sumNum: o.sumNum,
                drawTime: o.drawTime,
                drawIssue: o.drawIssue,
                preDrawIssue: o.preDrawIssue
            };
            if (t.flag) $(".nameLogo").find("img").attr("src", "http://kj.kai861.com/view/video/kuai3_video/img/logo/10007.png"), k3v.stopVideo(l, t);

            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    k3v.stopVideo(l, t)
                }, "1000")
            }
        }
    },
    fcsd: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            r = r < 0 ? 1 : r, o.cutime = r;
            o.drawTime.split(" ")[1];
            if (console.log(o), o.preDrawCode = i, t.flag) $(".logo").css("background", "url(http://kj.kai861.com/view/video/fc3DVideo/img/logo/10043.png) center center no-repeat"), fcsdv.startVid(o, t);
            else {
                if (t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    fcsdv.stopVid(o, t)
                }, "1000")
            }
        }
    },
    bjkl8: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            if (r = r < 0 ? 1 : r, o.cutime = r, console.log(o), o.preDrawCode = i, t.flag) $(".logo").css("background", "url(http://kj.kai861.com/view/video/bjkl8Video/img/azxy20.png) center center no-repeat"), syxwV.startVid(o, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    syxwV.stopVid(o, t)
                }, "1000")
            }
        }
    },
    twbg: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            if (r = r < 0 ? 1 : r, o.cutime = r, console.log(o), o.preDrawCode = i, t.flag) syxwV.startVid(o, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    syxwV.stopVid(o, t)
                }, "1000")
            }
        }
    },
    gxklsf: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data;
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            if (r = r < 0 ? 1 : r, o.cutime = r, console.log(o), o.numArr = i, t.flag) gxklsf.startVid(o, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    gxklsf.stopVid(o, t)
                }, "1000")
            }
        }
    },
    jisuft: function(e, t) {
        var o = pubmethod.tools.ifObj(e);
        if ("100002" == o.result.businessCode) throw new Error("error");
        if (0 == o.errorCode && 0 == o.result.businessCode) {
            o = o.result.data, $("#status").css("background-image", "http://kj.kai861.com/view/video/jisuft_video/images/logo_10035.png"), $(".logo").find("img").attr("src", "images/logo_" + o.lotCode + ".png");
            for (var r = pubmethod.tools.cutTime(o.drawTime, o.serverTime), s = o.preDrawCode.split(","), i = [], u = 0, n = s.length; u < n; u++) "0" == s[u].substr(0, 1) ? i.push(1 * s[u].substr(1, 1)) : i.push(1 * s[u]);
            if (r = r < 0 ? 1 : r, o.cutime = r, console.log(o), showcurrentresult(o.preDrawCode), $("#currentdrawid").text(o.drawCount), $("#nextdrawid").text(o.preDrawIssue), $("#stat1_1").text(o.sumFS), $("#stat1_2").text("0" == o.sumBigSamll ? "憭�" : "撠�"), $("#stat1_3").text("0" == o.sumSingleDouble ? "���" : "���"), $("#stat2_1").text("0" == o.firstDT ? "樴�" : "���"), $("#stat2_2").text("0" == o.secondDT ? "樴�" : "���"), $("#stat2_3").text("0" == o.thirdDT ? "樴�" : "���"), $("#stat2_4").text("0" == o.fourthDT ? "樴�" : "���"), $("#stat2_5").text("0" == o.fifthDT ? "樴�" : "���"), t.flag) startcountdown(r, t);
            else {
                if (!t.flag && r <= 1) throw new Error("error");
                setTimeout(function() {
                    finishgame(i.toString())
                }, "1000"), setTimeout(function() {
                    startcountdown(r - 11, t)
                }, "10000")
            }
        }
    }
};