<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/get_try2','TryController@get_try2');


Route::get('/get_try_pk10','TryController@get_try_pk10');
Route::get('/get_try_jisuft','TryController@get_try_jisuft');
Route::get('/get_try_ssc','TryController@get_try_ssc');
Route::get('/get_try_klsf','TryController@get_try_klsf');
Route::get('/get_try_shiyi5','TryController@get_try_shiyi5');
Route::get('/get_try_kuai3','TryController@get_try_kuai3');
Route::get('/get_try_bjkl8','TryController@get_try_bjkl8');
Route::get('/get_try_cqnc','TryController@get_try_cqnc');
Route::get('/get_try_gxklsf','TryController@get_try_gxklsf');
Route::get('/get_try_xgc','TryController@get_try_xgc');
Route::get('/get_try_fc3d','TryController@get_try_fc3d');
Route::get('/get_try_egxy','TryController@get_try_egxy');







//傳遞遊戲參數路由
Route::get('/get_Lotterydata_pk10','TryController@get_Lotterydata_pk10');
Route::get('/get_Lotterydata_jisuft','TryController@get_Lotterydata_jisuft');
Route::get('/get_Lotterydata_ssc','TryController@get_Lotterydata_ssc');
Route::get('/get_Lotterydata_klsf','TryController@get_Lotterydata_klsf');
Route::get('/get_Lotterydata_shiyi5','TryController@get_Lotterydata_shiyi5');
Route::get('/get_Lotterydata_kuai3','TryController@get_Lotterydata_kuai3');
Route::get('/get_Lotterydata_bjkl8','TryController@get_Lotterydata_bjkl8');
Route::get('/get_Lotterydata_cqnc','TryController@get_Lotterydata_cqnc');
Route::get('/get_Lotterydata_gxklsf','TryController@get_Lotterydata_gxklsf');
Route::get('/get_Lotterydata_xgc','TryController@get_Lotterydata_xgc');
Route::get('/get_Lotterydata_fc3d','TryController@get_Lotterydata_fc3d');
Route::get('/get_Lotterydata_egxy','TryController@get_Lotterydata_egxy');

