<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TryController extends Controller
{
    //賽車遊戲
    public function get_try_pk10(){
    	return view('get_try_pk10');
    }
    //天津財財彩
    public function get_try_ssc(){
    	return view('get_try_ssc');
    }
    //廣東快樂十分
    public function get_try_klsf(){
    	return view('get_try_klsf');
    }
    //廣東11選5
    public function get_try_shiyi5(){
    	return view('get_try_shiyi5');
    }
    //江蘇快3
    public function get_try_kuai3(){
    	return view('get_try_kuai3');
    }
    //急速快艇
    public function get_try_jisuft(){
    	return view('get_try_jisuft');
    }
    //重慶幸運農場
    public function get_try_cqnc(){
    	return view('get_try_cqnc');
    }
    //測試用
    public function get_try2(){

    	return view('get_try2');
    }
    //澳洲幸運20
    public function get_try_bjkl8(){
    	return view('get_try_bjkl8');
    }
    //廣西快樂十分
    public function get_try_gxklsf(){
        return view('get_try_gxklsf');
    }
    //香港六合彩
    public function get_try_xgc(){
        return view('get_try_xgc');
    }
    //排列3
    public function get_try_fc3d(){
        return view('get_try_fc3d');
    }
    //PC蛋蛋
    public function get_try_egxy(){
        return view('get_try_egxy');
    }
    //賽車遊戲傳遞的參數
    public function get_Lotterydata_pk10(){
    	date_default_timezone_set('Asia/Taipei');
		
		$get_now = date('Y-m-d H:i:s');
		$get_drawTime = date('Y-m-d H:i:s',strtotime('+15 second'));
		$preDrawTime = date('Y-m-d H:i:s',strtotime('+10 second'));

    	$data = array(
    		'errorCode' => '0',
    		'message'=>'操作成功',
    		'result'=>['businessCode'=>'0',
    		'message'=>'操作成功',
    		'data'=>[
    		'preDrawIssue'=>'666666',
    		'firstNum'=>'1',
    		'secondNum'=>'2',
    		'thirdNum'=>'3',
    		'fourthNum'=>'4',
    		'fifthNum'=>'5',
    		'sixthNum'=>'6',
    		'seventhNum'=>'7',
    		'eighthNum'=>'8',
    		'ninthNum'=>'9',
    		'tenthNum'=>'10',
    		'preDrawCode'=>'01,02,03,04,05,06,07,08,09,10',
    		'drawIssue'=>'31058433',
    		'drawTime'=>$get_drawTime,
    		'preDrawTime'=>$preDrawTime,
    		'preDrawDate'=>'2019-05-07',
    		'drawCount'=>'666',
    		'sumSingleDouble'=>'1',
    		'firstDT'=>'0',
    		'secondDT'=>'1',
    		'fourthDT'=>'0',    		
    		'fifthDT'=>'0',	//
    		'sumBigSamll'=>'0',
    		'sumFS'=>'18',
    		'thirdDT'=>'1',
    		'serverTime'=>$get_now,
    		'frequency'=>'',
    		'lotCode'=>'00000',
    		'iconUrl'=>'',
    		'shelves'=>'1',
    		'groupCode'=>'1',
    		'lotName'=>'模擬賽車',
    		'totalCount'=>'1152',
    		'index'=>'100'
    	]]); 
    	return json_encode($data);

    }
    //急速快艇
    public function get_Lotterydata_jisuft(){
    	date_default_timezone_set('Asia/Taipei');
		
		$get_now = date('Y-m-d H:i:s');
		$get_drawTime = date('Y-m-d H:i:s',strtotime('+15 second'));
		$preDrawTime = date('Y-m-d H:i:s',strtotime('+10 second'));

    	$data = array(
    		'errorCode' => '0',
    		'message'=>'操作成功',
    		'result'=>['businessCode'=>'0',
    		'message'=>'操作成功',
    		'data'=>[
    		'preDrawIssue'=>'666666',
    		'firstNum'=>'5',
    		'secondNum'=>'6',
    		'thirdNum'=>'4',
    		'fourthNum'=>'7',
    		'fifthNum'=>'3',
    		'sixthNum'=>'8',
    		'seventhNum'=>'2',
    		'eighthNum'=>'9',
    		'ninthNum'=>'1',
    		'tenthNum'=>'10',
    		'preDrawCode'=>'05,06,04,07,03,08,02,09,01,10',
    		'drawIssue'=>'31058433',
    		'drawTime'=>$get_drawTime,
    		'preDrawTime'=>$preDrawTime,
    		'preDrawDate'=>'2019-05-07',
    		'drawCount'=>'666',
    		'sumSingleDouble'=>'1',
    		'firstDT'=>'0',
    		'secondDT'=>'1',
    		'fourthDT'=>'0',    		
    		'fifthDT'=>'0',	//
    		'sumBigSamll'=>'0',
    		'sumFS'=>'18',
    		'thirdDT'=>'1',
    		'serverTime'=>$get_now,
    		'frequency'=>'',
    		'lotCode'=>'00005',
    		'iconUrl'=>'',
    		'shelves'=>'0',
    		'groupCode'=>'35',
    		'lotName'=>'急速快艇',
    		'totalCount'=>'1152',
    		'index'=>'100'
    	]]); 
    	return json_encode($data);

    }
    //天津財財彩傳遞的參數
    public function get_Lotterydata_ssc(){
    	date_default_timezone_set('Asia/Taipei');
		
		$get_now = date('Y-m-d H:i:s');
		$get_drawTime = date('Y-m-d H:i:s',strtotime('+50 second'));
		$preDrawTime = date('Y-m-d H:i:s',strtotime('+1 minutes'));

    	$data = array(
    		'errorCode' => '0',
    		'message'=>'操作成功',
    		'result'=>[
    		'businessCode'=>'0',
    		'message'=>'操作成功',
    		'data'=>[
    		'preDrawIssue'=>'666666',
    		'firstNum'=>'1',//骰子1~5(參數為1~9)
    		'secondNum'=>'2',
    		'thirdNum'=>'6',
    		'fourthNum'=>'4',
    		'fifthNum'=>'8',
    		'preDrawCode'=>'5,6,7,8,9',//數字骰子1~5(參數為0~9)
    		'drawIssue'=>'20190330002',
    		'drawTime'=>$get_drawTime,
    		'preDrawTime'=>$preDrawTime,
    		'preDrawDate'=>'2020-04-30 00:00:00',
    		'drawCount'=>'1',    		
    		'sumNum'=>'24',//(期數??)
    		'sumBigSmall'=>'0',//(0小、1大)
    		'dragonTiger'=>'2',//(1龍、2虎)
    		'sumSingleDouble'=>'1',//(0單、1雙)
    		'behindThree'=>'1',
    		'betweenThree'=>'1',
    		'firstBigSmall'=>'0',//第一個大小骰子
    		'firstSingleDouble'=>'0',//第一個單雙骰子
    		'secondBigSmall'=>'0',
    		'secondSingleDouble'=>'0',
    		'thirdBigSmall'=>'0',
    		'thirdSingleDouble'=>'0',
    		'fourthBigSmall'=>'0',
    		'fourthSingleDouble'=>'0',  		
    		'fifthBigSmall'=>'0',
    		'fifthSingleDouble'=>'0',
    		'lastThree'=>'0',	
    		'sdrawCount'=>'',
    		'id'=>'417526',
    		'status'=>'0',
    		'frequency'=>'',    				
    		'lotCode'=>'00001',
    		'iconUrl'=>'',
    		'shelves'=>'0',
    		'groupCode'=>'2',
    		'lotName'=>'模擬重慶財財彩',
    		'totalCount'=>'59',
    		'serverTime'=>$get_now,    
    		'index'=>'100'
    	]]); 
    	return json_encode($data);

    }
    //廣東快樂十分傳遞的參數
    public function get_Lotterydata_klsf(){
    	date_default_timezone_set('Asia/Taipei');
		
		$get_now = date('Y-m-d H:i:s');
		$get_drawTime = date('Y-m-d H:i:s',strtotime('+30 second'));
		$preDrawTime = date('Y-m-d H:i:s',strtotime('+1 minutes'));

    	$data = array(
    		'errorCode' => '0',
    		'message'=>'操作成功',
    		'result'=>[
    		'businessCode'=>'0',
    		'message'=>'操作成功',
    		'data'=>[
    		'preDrawIssue'=>'666666',
    		'preDrawCode'=>'01,02,03,04,05,06,19,20',
    		'lastBigSmall'=>'0',
    		'sumBigSmall'=>'0',
    		'sumNum'=>'86',
    		'firstDragonTiger'=>'1',
    		'secondDragonTiger'=>'1',
    		'thirdDragonTiger'=>'1',
    		'fourthDragonTiger'=>'1',
    		'drawIssue'=>'31058433',
    		'drawTime'=>$get_drawTime,
    		'preDrawTime'=>$preDrawTime,
    		'drawCount'=>'666',
    		'sumSingleDouble'=>'1',
    		'serverTime'=>$get_now,
    		'frequency'=>'',
    		'lotCode'=>'00002',
    		'iconUrl'=>'',
    		'shelves'=>'0',
    		'groupCode'=>'3',
    		'lotName'=>'廣東快樂十分',
    		'totalCount'=>'1152',
    		'index'=>'100'
    	]]); 
    	return json_encode($data);
	}
	//廣東11選5
	public function get_Lotterydata_shiyi5(){
		date_default_timezone_set('Asia/Taipei');
		
		$get_now = date('Y-m-d H:i:s');
		$get_drawTime = date('Y-m-d H:i:s',strtotime('+50 second'));
		$preDrawTime = date('Y-m-d H:i:s',strtotime('30 second'));

    	$data = array(
    		'errorCode' => '0',
    		'message'=>'操作成功',
    		'result'=>[
    		'businessCode'=>'0',
    		'message'=>'操作成功',
    		'data'=>[
    		'preDrawIssue'=>'666666',
    		'firstNum'=>'1',
    		'secondNum'=>'2',
    		'thirdNum'=>'3',
    		'fourthNum'=>'4',
    		'fifthNum'=>'5',
    		'preDrawCode'=>'01,02,03,04,05',
    		'drawIssue'=>'31058433',
    		'drawTime'=>$get_drawTime,
    		'preDrawTime'=>$preDrawTime,
    		'preDrawDate'=>'2019-05-08',
    		'drawCount'=>'666',
    		'sumNum'=>'24',
    		'sumBigSmall'=>'1',
    		'sumSingleDouble'=>'1',    		
    		'behindThree'=>'1',
    		'betweenThree'=>'1',    		
    		'dragonTiger'=>'0',	
    		'firstBigSmall'=>'1',
    		'firstSingleDouble'=>'1',
    		'secondBigSmall'=>'1',
    		'secondSingleDouble'=>'0',
    		'thirdBigSmall'=>'0',
    		'thirdSingleDouble'=>'1',
    		'fourthBigSmall'=>'1',
    		'fourthSingleDouble'=>'0',
    		'fifthBigSmall'=>'0',
    		'fifthSingleDouble'=>'1',
    		'lastThree'=>'0',
    		'enable'=>'',
    		'id'=>'266826',
    		'serverTime'=>$get_now,
    		'frequency'=>'',
    		'lotCode'=>'00003',
    		'iconUrl'=>'',
    		'shelves'=>'0',
    		'groupCode'=>'6',
    		'lotName'=>'廣東11選5',
    		'totalCount'=>'42',
    		'index'=>'100'
    	]]); 
    	return json_encode($data);
	}
	//江蘇快3
	public function get_Lotterydata_kuai3(){
		date_default_timezone_set('Asia/Taipei');
		
		$get_now = date('Y-m-d H:i:s');
		$get_drawTime = date('Y-m-d H:i:s',strtotime('+50 second'));
		$preDrawTime = date('Y-m-d H:i:s',strtotime('30 second'));

    	$data = array(
    		'errorCode' => '0',
    		'message'=>'操作成功',
    		'result'=>[
    		'businessCode'=>'0',
    		'message'=>'操作成功',
    		'data'=>[
    		'preDrawIssue'=>'666666',
    		'firstSeafood'=>'6',
    		'secondSeafood'=>'6',
    		'thirdSeafood'=>'6',
    		'preDrawCode'=>'6,6,6',
    		'drawIssue'=>'31058433',
    		'drawTime'=>$get_drawTime,
    		'preDrawTime'=>$preDrawTime,
    		'drawCount'=>'666',
    		'sumNum'=>'18',
    		'sumBigSmall'=>'1',
    		'sumSingleDouble'=>'0',
    		'serverTime'=>$get_now,
    		'frequency'=>'',
    		'lotCode'=>'00004',
    		'iconUrl'=>'',
    		'shelves'=>'0',
    		'groupCode'=>'5',
    		'lotName'=>'江蘇快3',
    		'totalCount'=>'41',
    		'index'=>'100'
    	]]); 
    	return json_encode($data);
	}
	//澳洲幸運20
	public function get_Lotterydata_bjkl8(){
		date_default_timezone_set('Asia/Taipei');
		
		$get_now = date('Y-m-d H:i:s');
		$get_drawTime = date('Y-m-d H:i:s',strtotime('+50 second'));
		$preDrawTime = date('Y-m-d H:i:s',strtotime('30 second'));

    	$data = array(
    		'errorCode' => '0',
    		'message'=>'操作成功',
    		'result'=>[
    		'businessCode'=>'0',
    		'message'=>'操作成功',
    		'data'=>[
    		'preDrawIssue'=>'666666',
    		'preDrawCode'=>'01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,2',//前面1~20的參數為01~80號,最後一個參數為1x~5x和10x參數為1~5和10
    		'drawIssue'=>'31058433',
    		'drawTime'=>$get_drawTime,
    		'preDrawTime'=>$preDrawTime,
    		'preDrawDate'=>'2019-05-17',
    		'drawCount'=>'201',
    		'sumBigSmall'=>'1',
    		'sumNum'=>'862',
    		'sumSingleDouble'=>'-1',
    		'sdrawCount'=>'',
    		'singleDoubleCount'=>'1',
    		'frontBehindCount'=>'-1',    		
    		'sumBsSd'=>'2',	//
    		'sumWuXing'=>'4',
    		'status'=>'0',
    		'thirdDT'=>'1',
    		'serverTime'=>$get_now,
    		'frequency'=>'',
    		'lotCode'=>'00006',
    		'iconUrl'=>'1',
    		'shelves'=>'1',
    		'groupCode'=>'7',
    		'lotName'=>'澳洲幸運20',
    		'totalCount'=>'288',
    		'index'=>'100'
    	]]); 
    	return json_encode($data);
	}
	//重慶幸運農場
	public function get_Lotterydata_cqnc(){
		date_default_timezone_set('Asia/Taipei');
		
		$get_now = date('Y-m-d H:i:s');
		$get_drawTime = date('Y-m-d H:i:s',strtotime('+50 second'));
		$preDrawTime = date('Y-m-d H:i:s',strtotime('30 second'));

    	$data = array(
    		'errorCode' => '0',
    		'message'=>'操作成功',
    		'result'=>[
    		'businessCode'=>'0',
    		'message'=>'操作成功',
    		'data'=>[
    		'preDrawIssue'=>'666666',
    		'preDrawCode'=>'01,02,08,09,13,14,19,20',//01~20
    		'drawIssue'=>'31058433',
    		'drawTime'=>$get_drawTime,
    		'preDrawTime'=>$preDrawTime,
    		'drawCount'=>'14',
    		'sumNum'=>'81',
    		'firstDragonTiger'=>'0',
    		'secondDragonTiger'=>'1',
    		'thirdDragonTiger'=>'0',    		
    		'fourthDragonTiger'=>'1',    		
    		'lastBigSmall'=>'1',
    		'sumBigSmall'=>'1',
    		'sumSingleDouble'=>'0',    		
    		'serverTime'=>$get_now,
    		'frequency'=>'',
    		'lotCode'=>'00007',
    		'iconUrl'=>'',
    		'shelves'=>'0',
    		'groupCode'=>'4',
    		'lotName'=>'重慶幸運農場',
    		'totalCount'=>'59',
    		'index'=>'100'
    	]]); 
    	return json_encode($data);
	}
    //廣西快樂十分
    public function get_Lotterydata_gxklsf(){
        date_default_timezone_set('Asia/Taipei');
        
        $get_now = date('Y-m-d H:i:s');
        $get_drawTime = date('Y-m-d H:i:s',strtotime('+50 second'));
        $preDrawTime = date('Y-m-d H:i:s',strtotime('30 second'));

        $data = array(
            'errorCode' => '0',
            'message'=>'操作成功',
            'result'=>[
            'businessCode'=>'0',
            'message'=>'操作成功',
            'data'=>[
            'preDrawIssue'=>'666666',
            'preDrawCode'=>'7,7,7,7,7',//5個參數,號碼1~20,可以重複
            'drawIssue'=>'31058433',
            'drawTime'=>$get_drawTime,
            'preDrawTime'=>$preDrawTime,
            'preDrawDate'=>'2019-05-08',
            'drawCount'=>'666',
            'sumNum'=>'52',
            'sumBigSmall'=>'1',
            'sumSingleDouble'=>'1',       
            'firstDragonTiger'=>'1', 
            'serverTime'=>$get_now,
            'frequency'=>'',
            'lotCode'=>'00008',
            'iconUrl'=>'',
            'shelves'=>'0',
            'groupCode'=>'8',
            'lotName'=>'廣東快樂十分',
            'totalCount'=>'50',
            'index'=>'100'
        ]]); 
        return json_encode($data);
    }
    //香港六合彩
    public function get_Lotterydata_xgc(){
        date_default_timezone_set('Asia/Taipei');
        
        $get_time = date('H:i:s');
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $get_drawTime = date('Y-m-d H:i:s',strtotime('+10 second'));
        //$preDrawTime = date('Y-m-d H:i:s',strtotime('30 second'));

        $data = array(
            'id' => '01',
            'time'=>$get_time,
            'nextid'=>'02',
            's'=>'0',
            'c'=>'4001',
            'ma'=>'6,羊,red,12,羊,green,18,猴,blue,24,龍,green,30,鼠,red,36,雞,blue,42,狗,red',//
            'year'=>$year,
            'm'=>$month,
            'day'=>$day,
            'type'=>'4',
            'nextdate'=>$get_drawTime,
            'info'=>''
        ); 
        return json_encode($data);
    }
    //排列3
    public function get_Lotterydata_fc3d(){
        date_default_timezone_set('Asia/Taipei');
        
        $get_now = date('Y-m-d H:i:s');
        $get_drawTime = date('Y-m-d H:i:s',strtotime('+50 second'));
        $preDrawTime = date('Y-m-d H:i:s',strtotime('30 second'));

        $data = array(
            'errorCode' => '0',
            'message'=>'操作成功',
            'result'=>[
            'businessCode'=>'0',
            'message'=>'操作成功',
            'data'=>[
            'preDrawIssue'=>'666666',
            'sjh'=>'3,9,5',               
            'preDrawCode'=>'8,8,8',        
            'drawIssue'=>'31058433',
            'drawTime'=>$get_drawTime,
            'preDrawTime'=>$preDrawTime,
            'sumBigSmall'=>'1',
            'sumNum'=>'7',
            'sumSingleDouble'=>'0',
            'sumHundredTen'=>'6',
            'htSingleDouble'=>'1',
            'httailBigSmall'=>'0',
            'sumHundredOne'=>'1',
            'hoSingleDouble'=>'0',
            'hotailBigSmall'=>'1',
            'sumTenOne'=>'7',
            'toSingleDouble'=>'0',
            'totailBigSmall'=>'0',
            'serverTime'=>$get_now,
            'frequency'=>'',
            'lotCode'=>'00009',
            'iconUrl'=>'',
            'shelves'=>'0',
            'groupCode'=>'41',
            'lotName'=>'排列3',
            'totalCount'=>'1',
            'index'=>'100'
        ]]); 
        return json_encode($data);
    }
    //PC蛋蛋
    public function get_Lotterydata_egxy(){
        date_default_timezone_set('Asia/Taipei');
        
        $get_now = date('Y-m-d H:i:s');
        $get_drawTime = date('Y-m-d H:i:s',strtotime('+50 second'));
        $preDrawTime = date('Y-m-d H:i:s',strtotime('30 second'));

        $data = array(
            'errorCode' => '0',
            'message'=>'操作成功',
            'result'=>[
            'businessCode'=>'0',
            'message'=>'操作成功',
            'data'=>[
            'preDrawIssue'=>'666666',       
            'preDrawCode'=>'5,5,5',        
            'drawIssue'=>'31058433',
            'drawTime'=>$get_drawTime,
            'preDrawTime'=>$preDrawTime,
            'drawCount'=>'98',
            'sumBigSmall'=>'1',
            'sumNum'=>'15',
            'sumSingleDouble'=>'1',
            'sdrawCount'=>'',
            'sumMiddle'=>'1',
            'sumLimit'=>'1',
            'status'=>'0',
            'serverTime'=>$get_now,
            'frequency'=>'',
            'lotCode'=>'00010',
            'iconUrl'=>'',
            'shelves'=>'0',
            'groupCode'=>'46',
            'lotName'=>'PC幸運蛋蛋',
            'totalCount'=>'179',
            'index'=>'100'
        ]]); 
        return json_encode($data);
    }
}
