
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title></title>
		<link rel="stylesheet" href="css/gdklsf.css" />
		<link rel="stylesheet" href="fonts/fonts.css" />
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/gdklsf.js"></script>
		<script src="js/config.js"></script>
		<script src="js/video.js"></script>
	</head>

	<body>
		<div class="loading" style="background: #000;width:100%;height:100%;color:#fff;position:absolute;top:0;left:0;z-index: 99999999; text-align: center;font-size: 20px;padding-top: 30%;">LOADING...</div>
		<div class="video_box">
			<div id="az8VideoBox">
				<div class="logo"></div>
				<div class="center_box">
					<div>
						<ul id="rethtml">
							<!--<li class="blue ball">10</li>
							<li class="blue ball">08</li>
							<li class="blue ball">06</li>
							<li class="blue ball">20</li>
							<li class="blue ball">25</li>
							<li class="blue ball">16</li>
							<li class="red ball">46</li>
							<li class="blue ball">23</li>-->
						</ul>
					</div>
					<p class="ThisIssue">當前期 : <span id="thisIss"> </span></p>
					<div class="opening opentyle" style="display: none;">開獎中...</div>
					<div class="Time_box">00:00:00 </div>
				</div>
				<div class="right_box">
					<div class="btnBox">
						<button id="btnsound"></button>
					</div>
					<div class="Tibox">
						<p>下期：<span id="nextIssue"> </span></p>
						<p>開獎： <span id="nextOpTime"> </span></p>
					</div>
				</div>
				<div class="middle_box">
					<ul class="move_ballUl ">
						<li class="ball light_skyblue 1">01</li>
						<li class="ball light_skyblue 2">02</li>
						<li class="ball light_skyblue 3">03</li>
						<li class="ball light_skyblue 4">04</li>
						<li class="ball light_skyblue 5">05</li>
						<li class="ball light_skyblue 6">06</li>
						<li class="ball light_skyblue 7">07</li>
						<li class="ball light_skyblue 8">08</li>
						<li class="ball light_skyblue 9">09</li>
						<li class="ball light_skyblue 10">10</li>
						<li class="ball light_skyblue 11">11</li>
						<li class="ball light_skyblue 12">12</li>
						<li class="ball light_skyblue 13">13</li>
						<li class="ball light_skyblue 14">14</li>
						<li class="ball light_skyblue 15">15</li>
						<li class="ball light_skyblue 16">16</li>
						<li class="ball light_skyblue 17">17</li>
						<li class="ball light_skyblue 18">18</li>
						<li class="ball red 19">19</li>
						<li class="ball red 20">20</li>
					</ul>
			</div><!-- az8VideoBox -->
			<div class="posBG"></div>
			<div class="bottom_box">
				<button class="kaiBtn"></button>
				<div class="result_box">
					<ul>
						<!--<li class="ball blue ">12</li>
		    			<li class="ball blue">03</li>
		    			<li class="ball blue">05</li>
		    			<li class="ball blue">66</li>
		    			<li class="ball blue">78</li>
		    			<li class="ball blue">21</li>
		    			<li class="ball blue">22</li>
		    			<li class="ball blue">56</li>-->
					</ul>
				</div>
			</div>
		</div>
		</div>
		<div class="soundbox">
			<audio src="http://kj.kai861.com/view/video/GDklsf/sound/klsf-1.mp3" id="bgsound" loop="loop"></audio>
			<audio src="http://kj.kai861.com/view/video/GDklsf/sound/klsf-2.mp3" id="kaisound" loop="loop"></audio>
		</div>
		<script type="text/javascript">
			var pW = $("html").width();
			var zoom = pW / 1625;
			var h = zoom * 750;
			var ty = navigator.userAgent.toLowerCase();
			if(ty.indexOf("trident") != -1 || ty.indexOf("firefox") != -1) {
				$("html").css({
					"transform-origin": "top left",
					"transform": "scale(" + zoom + ")"
				});
			} else {
				$("html").css({
					"zoom": zoom + ""
				});
			}
			try {
				$("iframe", window.parent.document).width(pW);
				$("iframe", window.parent.document).height(h);
			} catch(e) {

			}
		</script>
	</body>

</html>