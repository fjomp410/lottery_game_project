
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title></title>
		<link rel="stylesheet" href="css/cqnc_video.css" />
		<link rel="stylesheet" href="fonts/fonts.css" />
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/cqnc.js"></script>
		<script src="js/config.js"></script>
		<script src="js/video.js"></script>
	</head>

	<body>
		<div class="loading" style="background: #000;width:100%;height:100%;color:#fff;position:absolute;top:0;left:0;z-index: 99999999; text-align: center;font-size: 20px;padding-top: 30%;">LOADING...</div>
		<div id="videobox">
			<div class="content">
				<!--<div class="head">
					重庆幸运农场开奖视频
					<div class="btn">
						<ul>
							<li class="closevideo"><i class="iconfont"></i></li>
							<li class="small">小屏</li>
							<li class="big">中屏</li>
						</ul>
					</div>
				</div>-->
				<div class="animate">

					<div class="cqncAnimate">
						<div class="logo"></div>
						<div class="bodybg"><img src="http://kj.kai861.com/view/video/cqnc/img/cqnc_video.jpg" /></div>
						<!--<div class="loading">
							<div class="loadtxt">
								LOADING...
							</div>
						</div>-->
						<div class="position">
							<div class="current_code">
								<span>第<span class="codeNow"></span>期</span>
							</div>
							<div class="kaiNum">
								<ul>
									<li class=""></li>
									<li class=""></li>
									<li class=""></li>
									<li class=""></li>
									<li class=""></li>
									<li class=""></li>
									<li class=""></li>
									<li class=""></li>
								</ul>
								<span class="soundsIcon on"></span>
							</div>
						</div>
						<div class="numCircle">
							<ul class="circle_t">
								<li class="roll_1 "></li>
								<li class="roll_2"></li>
								<li class="roll_3 "></li>
								<li class="roll_4 "></li>
								<li class="roll_5"></li>
								<li class="roll_6"></li>
								<li class="roll_7 "></li>
							</ul>
							<ul class="circle_r">
								<li class="roll_8"></li>
								<li class="roll_9 "></li>
								<li class="roll_10"></li>
							</ul>
							<ul class="circle_d">
								<li class="roll_11"></li>
								<li class="roll_12"></li>
								<li class="roll_13 "></li>
								<li class="roll_14"></li>
								<li class="roll_15"></li>
								<li class="roll_16 "></li>
								<li class="roll_17"></li>
							</ul>
							<ul class="circle_l">
								<li class="roll_18 "></li>
								<li class="roll_19"></li>
								<li class="roll_20"></li>
							</ul>
						</div>
						<div class="detail">
							<div class="detail_l">
								<div class="nowIssue">本期<span></span></div>
								<ul class="numlist">
									<li></li>
									<li></li>
									<li></li>
									<li></li>
									<li></li>
									<li></li>
									<li></li>
									<li></li>
								</ul>
								<div class="sum">總和&nbsp;:&nbsp;<span></span></div>
								<div class="longhu">
									<span class="danxu"></span>
									<span class="daxiao"></span>
									<span class="weixiao"></span>
									<!--<span>虎</span>-->
								</div>
							</div>
							<div class="detail_r">
								<span class="cutTime">開獎倒數時</span>
								<span class="timeing">00：00：00</span>
								<span class="startBtn">試試手氣</span>
								<p id="mnkai_text">即將開獎,請稍後</p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		<script type="text/javascript">
			var pW = $("html").width();
			var zoom = pW / 1625;
			var h = zoom * 780;
			var ty = navigator.userAgent.toLowerCase();
			if(ty.indexOf("trident") != -1 || ty.indexOf("firefox") != -1) {
				$("html").css({
					"transform-origin": "top left",
					"transform": "scale(" + zoom + ")"
				});
			} else {
				$("html").css({
					"zoom": zoom + ""
				});
			}
			try {
				$("iframe", window.parent.document).width(pW);
				$("iframe", window.parent.document).height(h);
			} catch(e) {

			}
		</script>
		<div style="display: none;">
			<audio id="bgsound" loop="loop">
				<source src="sound/bg.mp3" type="audio/mpeg" loop="loop" />
			</audio>
			<audio id="kaisound" loop="loop">
				<source src="sound/kaijiin.mp3" type="audio/mpeg" />
			</audio>
			<audio id="emptsound" loop="loop">
				<source src="sound/empt.mp3" type="audio/mpeg" />
			</audio>
		</div>
		<!--<button id="btn">333</button>-->
	</body>

</html>