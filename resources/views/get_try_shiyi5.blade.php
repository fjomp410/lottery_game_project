
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title></title>
		<link rel="stylesheet" href="css/11x5.css" />
		<script src="js/jquery-1.7.2.min.js"></script>
		<script src="js/shiyi5_main.js"></script>
		<script src="js/config.js"></script>
		<script src="js/video.js"></script>
	</head>

	<body>
		<div id="videobox">
			<div class="content">
				<div class="animate">
					<div class="eleAnimate">
						<div class="loading">
							<div class="loadtxt">
								LOADING...
							</div>
						</div>
						<div class="noinfor"></div>
						<div class="bodybg"><img src="http://kj.kai861.com/view/video/11x5_video/img/flash.png" /></div>
						<div class="animate_content">
							<div class="nameLogo"><img src="http://kj.kai861.com/view/video/11x5_video/img/11x5_gd.png" alt="" /></div>
							<div class="codeArr">
								<ul>
									<li class="code6"></li>
									<li class="code7"></li>
									<li class="code8"></li>
									<li class="code9"></li>
									<li class="code10"></li>
									<li>
										<span class="code11">1</span>
										<span class="code12">小</span>
										<span class="code13">單</span>
									</li>
								</ul>
							</div>
							<div class="drawInfo">
								<div class="nextDraw">
									<p class="next">下期開獎<span id="drawIssue">2017051748</span></p>
									<p>開獎時間<span id="drawTime">00：00：00</span></p>
								</div>
								<div id="spanbtn" class="soundsOn"></div>
								<audio src="sound/bg.mp3" id="audioidB" loop="loop"></audio>
								<audio src="sound/run.mp3" id="audioidR" loop="loop"></audio>
							</div>
						</div>
						<div class="codeNum">
							<ul>
								<li class="num1"></li>
								<li class="num2"></li>
								<li class="num3"></li>
								<li class="num4"></li>
								<li class="num5"></li>
							</ul>
							<div id="hidNum">
								<span class="num1">1</span>
								<span class="num2">2</span>
								<span class="num3">3</span>
								<span class="num4">4</span>
								<span class="num5">5</span>
							</div>
						</div>
						<div class="manPic">
							<span class="manll"></span>
							<span class="manrl"></span>
						</div>
						<div class="cuttime">
							<div id="hourtxt">19:36:00</div>
							<div id="opening">正在開獎中</div>
						</div>
						<span id="mnKai"></span>
					</div>

				</div>
			</div>
		</div>
		<!--<div style="max-width: 400px;margin: 0 auto;">
			<input onclick="startVideo()" type="button" value="启动开奖视频" />
			<input onclick="stopVideo()" type="button" value="停止启动开奖" />
		</div>
		<script>
			var dataStr = {
				preDrawCode: [2, 4, 6, 4, 5],
				sumNum: 12,
				sumBigSmall: "小",
				sumSingleDouble: "单",
				drawIssue: "170517061",
				preDrawTime: "00:00:60"
			};
			function startVideo(){
				k3v.startVideo(dataStr);
			}
			function stopVideo(){
				k3v.stopVideo(dataStr)
			}
		</script>-->
		<script type="text/javascript">
			var pW = $("html").width();
			var zoom = pW / 1625;
			var h = zoom * 780;
			var ty = navigator.userAgent.toLowerCase();
			if(ty.indexOf("trident") != -1 || ty.indexOf("firefox") != -1) {
				$("html").css({
					"transform-origin": "top left",
					"transform": "scale(" + zoom + ")"
				});
			} else {
				$("html").css({
					"zoom": zoom + ""
				});
			}
			try {
				$("iframe", window.parent.document).width(pW);
				$("iframe", window.parent.document).height(h);
			} catch(e) {

			}
		</script>
	</body>

</html>