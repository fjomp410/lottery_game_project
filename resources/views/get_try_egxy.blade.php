
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title></title>
		<link rel="stylesheet" href="fonts/fonts.css" />
		<link rel="stylesheet" href="css/egxy.css" />
		<script src="js/jquery-1.7.2.min.js"></script>
		<script src="js/egxy.js"></script>
		<script src="js/config.js"></script>
		<script src="js/video.js"></script>
	</head>

	<body>
		<div class="loading" style="background: #000;width:100%;height:100%;color:#fff;position:absolute;top:0;left:0;z-index: 99999999; text-align: center;font-size: 20px;padding-top: 30%;">LOADING...</div>
		<div id="videobox">
			<div class="content">
				<!--<div class="head">
					福彩3D开奖视频
					<div class="btn">
						<ul>
							<li class="closevideo"><i class="iconfont"></i></li>
							<li class="small">小屏</li>
							<li class="big">中屏</li>
						</ul>
					</div>
				</div>-->
				<div class="animate">
					<div class="cqncAnimate">
						<div class="bodybg"><img src="http://kj.kai861.com/view/video/pcEgg_video/img/pcEgg_bg.png" /></div>
						<!--<div class="loading">
							<div class="loadtxt">
								LOADING...
							</div>
						</div>-->
						<div class="position">
							<div class="logo"><img src="http://kj.kai861.com/view/video/pcEgg_video/img/logo.png" alt="" /></div>
							<div class="kaiDiv">
								<ul id="kaiNum" class="numShow">
									<li>1</li>
									<li>2</li>
									<li>3</li>
								</ul>
							</div>
							<div class="defDiv">
								<div class="drawTimeDiv">
									<p class="next">下期：<span id="nextIssue">00000</span></p>
									<p class="draw">開獎：<span id="drawTime">00:00:00</span></p>
								</div>
								<span id="soundBth" class="soundsOn"></span>
								<audio src="sound/3dbj2.mp3" id="audioidBg" loop="loop"></audio>
								<audio src="sound/3d2.mp3" id="audioidKai" loop="loop"></audio>
							</div>
						</div>

						<div class="cutTime">
							<div>
								<span id="hourtxt">00:00:00</span>
								<span id="opening">開獎中...</span>
							</div>
						</div>
						<div class="kaiBox">
							<ul id="kaiUl">
								<li><span class="ball num1 orgin"></span></li>
								<li><span class="ball num2 orgin"></span></li>
								<li><span class="ball num3 orgin"></span></li>
							</ul>
						</div>
						<div class="lightBox">
							<ul id="light">
								<li></li>
								<li></li>
								<li></li>
							</ul>
						</div>
						<div class="tryKai">
							<span class="jzCheck" style="display: none;">開獎中，禁止模擬</span>
							<span id="tryBtn" onmousedown="mDown(this)" onmouseup="mUp(this)"></span>
						</div>
					</div>
				</div>
				<!--<button id="clickMe">点我</button>-->
			</div>
		</div>
	</body>
	<script type="text/javascript">
			var pW = $("html").width();
			var zoom = pW / 1625;
			var h = zoom * 750;
			var ty = navigator.userAgent.toLowerCase();
			if(ty.indexOf("trident") != -1 || ty.indexOf("firefox") != -1) {
				$("html").css({
					"transform-origin": "top left",
					"transform": "scale(" + zoom + ")"
				});
			} else {
				$("html").css({
					"zoom": zoom + ""
				});
			}
			try {
				$("iframe", window.parent.document).width(pW);
				$("iframe", window.parent.document).height(h);
			} catch(e) {

			}
		</script>

</html>