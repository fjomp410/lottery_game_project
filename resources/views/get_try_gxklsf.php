
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title></title>
		<link rel="stylesheet" href="fonts/fonts.css" />
		<link rel="stylesheet" href="css/gxklsf.css" />
		<script src="js/jquery-1.7.2.min.js"></script>
		<script src="js/gxklsf.js"></script>
		<script src="js/config.js"></script>
		<script src="js/video.js"></script>
	</head>

	<body>
		<div class="loading" style="background: #000;width:100%;height:100%;color:#fff;position:absolute;top:0;left:0;z-index: 99999999; text-align: center;font-size: 20px;padding-top: 30%;">LOADING...</div>
		
		<div id="videobox">
			<div class="content">
				<!--<div class="head">
					福彩3D开奖视频
					<div class="btn">
						<ul>
							<li class="closevideo"><i class="iconfont"></i></li>
							<li class="small">小屏</li>
							<li class="big">中屏</li>
						</ul>
					</div>
				</div>-->
				<div class="animate">
					<div class="cqncAnimate">
						<div class="bodybg"><img src="http://kj.kai861.com/view/video/gxklsf_video/img/background.png" /></div>
						<div class="loading">
							<!--<div class="loadtxt">
								LOADING...
							</div>-->
						</div>
						<div class="position">
							<div class="logo"><img src="http://kj.kai861.com/view/video/gxklsf_video/img/logo.png" alt="" /></div>
							<div class="kaiDiv">
								<ul id="kaiNum" class="numShow">
									<li>1</li>
									<li>2</li>
									<li>3</li>
									<li>4</li>
									<li>5</li>
								</ul>
							</div>
							<div class="defDiv">
								<div class="drawTimeDiv">
									<p class="next">下期：<span id="nextIssue">00000</span></p>
									<p class="draw">开奖：<span id="drawTime">00:00:00</span></p>
								</div>
								<span id="soundBth" class="soundsOn"></span>
								<audio src="sound/3dbj2.mp3" id="audioidBg" loop="loop"></audio>
								<audio src="sound/3d2.mp3" id="audioidKai" loop="loop"></audio>
								<!--	<audio src="" id="audioidBg" loop="loop"></audio>
								<audio src="" id="audioidKai" loop="loop"></audio>-->
							</div>
							<!--开奖号码区-->
							<div class="kaiArea">
								<div class="oneKaiBox kaiBox">
									<ul class="KaiUl oneUl">
										<li class='comNum'>1</li>
										<li class='comNum'>2</li>
										<li class='comNum'>3</li>
										<li class='comNum'>4</li>
										<li class='comNum'>5</li>
										<li class='comNum'>6</li>
										<li class='comNum'>1</li>
										<li class='comNum'>2</li>
										<li class='comNum'>3</li>
										<li class='comNum'>4</li>
										<li class='comNum'>5</li>
										<li class='comNum'>6</li>
										<li class='actNum'>1</li>
									</ul>
								</div>
								<div class="twoKaiBox kaiBox">
									<ul class="KaiUl twoUl">
										<li class='comNum'>1</li>
										<li class='comNum'>2</li>
										<li class='comNum'>3</li>
										<li class='comNum'>4</li>
										<li class='comNum'>5</li>
										<li class='comNum'>6</li>
										<li class='comNum'>1</li>
										<li class='comNum'>2</li>
										<li class='comNum'>3</li>
										<li class='comNum'>4</li>
										<li class='comNum'>5</li>
										<li class='comNum'>6</li>
										<li class='actNum'>7</li>
									</ul>
								</div>
								<div class="threeKaiBox kaiBox">
									<ul class="KaiUl threeUl">
										<li class='comNum'>1</li>
										<li class='comNum'>2</li>
										<li class='comNum'>3</li>
										<li class='comNum'>4</li>
										<li class='comNum'>5</li>
										<li class='comNum'>6</li>
										<li class='comNum'>1</li>
										<li class='comNum'>2</li>
										<li class='comNum'>3</li>
										<li class='comNum'>4</li>
										<li class='comNum'>5</li>
										<li class='comNum'>6</li>
										<li class='actNum'>7</li>
									</ul>
								</div>
								<div class="fourKaiBox kaiBox">
									<ul class="KaiUl fourUl">
										<li class='comNum'>1</li>
										<li class='comNum'>2</li>
										<li class='comNum'>3</li>
										<li class='comNum'>4</li>
										<li class='comNum'>5</li>
										<li class='comNum'>6</li>
										<li class='comNum'>1</li>
										<li class='comNum'>2</li>
										<li class='comNum'>3</li>
										<li class='comNum'>4</li>
										<li class='comNum'>5</li>
										<li class='comNum'>6</li>
										<li class='actNum'>7</li>
									</ul>
								</div>
								<div class="fiveKaiBox kaiBox">
									<ul class="KaiUl fiveUl">
										<li class='comNum'>1</li>
										<li class='comNum'>2</li>
										<li class='comNum'>3</li>
										<li class='comNum'>4</li>
										<li class='comNum'>5</li>
										<li class='comNum'>6</li>
										<li class='comNum'>1</li>
										<li class='comNum'>2</li>
										<li class='comNum'>3</li>
										<li class='comNum'>4</li>
										<li class='comNum'>5</li>
										<li class='comNum'>6</li>
										<li class='actNum'>7</li>
									</ul>
								</div>
							</div>
							<ul id="newKaiList" class="numShow">
								<li>1</li>
								<li>2</li>
								<li>3</li>
								<li>4</li>
								<li>5</li>
							</ul>
						</div>

						<div class="cutTime">
							<div>
								<span id="hourtxt">00:00:00</span>
								<span id="opening">開獎中...</span>
							</div>
						</div>
						<div class="tryKai">
							<span class="jzCheck" style="display: none;">開獎中,禁止模擬</span>
							<span id="tryBtn">開獎模擬</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			var pW = $("html").width();
			var zoom = pW / 1625;
			var h = zoom * 780;
			var ty = navigator.userAgent.toLowerCase();
			if(ty.indexOf("trident") != -1 || ty.indexOf("firefox") != -1) {
				$("html").css({
					"transform-origin": "top left",
					"transform": "scale(" + zoom + ")"
				});
			} else {
				$("html").css({
					"zoom": zoom + ""
				});
			}
			try {
				$("iframe", window.parent.document).width(pW);
				$("iframe", window.parent.document).height(h);
			} catch(e) {

			}
		</script>
	</body>

</html>