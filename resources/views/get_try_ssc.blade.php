
<!DOCTYPE html>
<html>

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,minimal-ui">
		<meta name="format-detection" content="telephone=no" />
		<title></title>
		<link rel="stylesheet" href="css/ssc.css" />
		<link rel="stylesheet" href="css/ssc_animateTool.css" />
		
	</head>

	<body>
		<div class="cqsscAnimate">
			<div class="bodybg"><img src="http://kj.kai861.com/view/video/SSC/img/cqssc/bodybg.jpg" /></div>
			<div class="loading">
				<div class="loadtxt">
					LOADING...
				</div>
			</div>
			<div class="content">
				<div id="hlogo" class="nameLogo nameLogo_cq">
					<img src="http://kj.kai861.com/view/video/SSC/img/cqssc/logo-10002.png" />
				</div>
				<div class="coderbox">
					<div class="codeboxl">
						<div class="line tl">
							<div class="box perspectiveView">
								<span class="flip afterbg out"></span>
								<span class="flip bigbg"></span>
							</div>
							<div class="box perspectiveView">
								<span class="flip afterbg out"></span>
								<span class="flip smallbg"></span>
							</div>
							<div class="box perspectiveView">
								<span class="flip afterbg out"></span>
								<span class="flip bigbg"></span>
							</div>
							<div class="box perspectiveView">
								<span class="flip afterbg out"></span>
								<span class="flip smallbg"></span>
							</div>
							<div class="box perspectiveView">
								<span class="flip afterbg out"></span>
								<span class="flip bigbg"></span>
							</div>
						</div>
						<div class="line ml" id="numBig">
							<div class="box beforebg">
								<span class="num"></span>
							</div>
							<div class="box beforebg">
								<span class="num"></span>
							</div>
							<div class="box beforebg">
								<span class="num"></span>
							</div>
							<div class="box beforebg">
								<span class="num"></span>
							</div>
							<div class="box beforebg">
								<span class="num"></span>
							</div>
						</div>
						<div class="line bl">
							<div class="box perspectiveView">
								<span class="flip afterbg out"></span>
								<span class="flip singlebg"></span>
							</div>
							<div class="box perspectiveView">
								<span class="flip afterbg out"></span>
								<span class="flip doublebg"></span>
							</div>
							<div class="box perspectiveView">
								<span class="flip afterbg out"></span>
								<span class="flip doublebg"></span>
							</div>
							<div class="box perspectiveView">
								<span class="flip afterbg out"></span>
								<span class="flip singlebg"></span>
							</div>
							<div class="box perspectiveView">
								<span class="flip afterbg out"></span>
								<span class="flip doublebg"></span>
							</div>
						</div>
					</div>
					<div class="codeboxr">
						<div class="heiban">
							<div class="line1" id="qishu">
								本期：<span class="redfont" id="preDrawIssue">20190508</span>期
							</div>
							<div class="line1" id="nexttime">
								<span>下期開獎：</span><span class="redfont" id="drawTime">--:--:--</span>
							</div>
							<div class="line1">
								<div class="oping">
									<div class="cuttimetitle">正在開獎...</div>
								</div>
								<div class="djs">
									<span class="cuttimetitle" id="cuttime">倒數：</span>
									<span class="bluefont"></span>
								</div>
							</div>
						</div>
						<div class="heibanb">
							<div class="bckj">
								<span id="sumNum">16</span><span id="sumSingleDouble">雙</span><span id="sumBigSmall">小</span><span id="dragonTiger">虎</span>
							</div>
							<div class="smallnum" id="litNum">
								<div class="box beforebg">
									<span class="num2"></span>
								</div>
								<div class="box beforebg">
									<span class="num1"></span>
								</div>
								<div class="box beforebg">
									<span class="num0"></span>
								</div>
								<div class="box beforebg">
									<span class="num9"></span>
								</div>
								<div class="box beforebg">
									<span class="num6"></span>
								</div>
							</div>
							<!--<div class="smallnum line" id="btnbox">
								<div class="box">
									<div class="orbtn">两面路珠</div>
								</div>
								<div class="box">
									<div class="orbtn"> 开奖历史</div>
								</div>
								<div class="box">
									<div class="orbtn"> 走势分析</div>
								</div>
							</div>-->
						</div>
					</div>
				</div>
				<div class="menubox">
					<div class="tyrbtn"><!-- 試試手氣 --></div>
					<div id="soundbtn" class="soundbtn"></div>
				</div>
				<div class="disnone">
					<!-- 音樂 -->
					<audio autoplay="autoplay" id="bgsound" src=""></audio>
				</div>
			</div>
		</div>
		
	</body>
	<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript">

			var pW = $("html").width();
			var zoom = pW / 1200;
			var h = zoom * 800;
			var ty = navigator.userAgent.toLowerCase();
			if(ty.indexOf("trident") != -1 || ty.indexOf("firefox") != -1) {
				$("html").css({
					"transform-origin": "top left",
					"transform": "scale(" + zoom + ")"
				});
			} else {
				$("html").css({
					"zoom": zoom + ""
				});
			}
			try {
				$("iframe", window.parent.document).width(pW);
				$("iframe", window.parent.document).height(h);
			} catch(e) {

			}
		</script>
	<script type="text/javascript" src="js/victor.js"></script>
	<script src="/js/config.js"></script>
	<script src="/js/video.js"></script>


</html>