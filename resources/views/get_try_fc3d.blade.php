
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title></title>
		<link rel="stylesheet" href="fonts/fonts.css" />
		<link rel="stylesheet" href="css/fc3d.css" />
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/fc3d.js"></script>
		<script src="js/config.js"></script>
		<script src="js/video.js"></script>
	</head>

	<body>
		<div class="loading" style="background: #000;width:100%;height:100%;color:#fff;position:absolute;top:0;left:0;z-index: 99999999; text-align: center;font-size: 20px;padding-top: 30%;">LOADING...</div>
		
		<div id="videobox">
			<div class="content">
				<!--<div class="head">
					福彩3D开奖视频
					<div class="btn">
						<ul>
							<li class="closevideo"><i class="iconfont"></i></li>
							<li class="small">小屏</li>
							<li class="big">中屏</li>
						</ul>
					</div>
				</div>-->
				<div class="animate">
					<div class="cqncAnimate">
						<div class="bodybg"><img src="http://kj.kai861.com/view/video/fc3DVideo/img/fc3D_bg.png" /></div>
						<div class="loading">
							<!--<div class="loadtxt">
								LOADING...
							</div>-->
						</div>
						<div class="position">
							<ul>
								<li class="logo"></li>
								<li class="issue">本期：<span id="issue">838601</span>&nbsp;期</li>
								<li class="headCode">
									<ul>
										<li id="oneCode"><span>1</span></li>
										<li id="twoCode"><span>2</span></li>
										<li id="threeCode"><span>3</span></li>
									</ul>
								</li>
								<li class="kaiTime">下期開獎：
									<span id="kaiTime">17:00:00</span>
								</li>
								<li id="soundBth" class="soundsOn"></li>
								<audio src="sound/3dbj2.mp3" id="audioidBg" loop="loop"></audio>
								<audio src="sound/3d2.mp3" id="audioidKai" loop="loop"></audio>
							</ul>
						</div>
						<div class="runCode">
							<ul id="curNumUl">
								<li class="firstBall">
									<span>1</span>
								</li>
								<li class="secondBall">
									<span>2</span>
								</li>
								<li class="thirdBall">
									<span>6</span>
								</li>
							</ul>
							<ul id="runNumUl">
								<li class="parLi firstBallPip">
									<ul class="leftUl">
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
									</ul>
									<ul class="rightUl">
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
									</ul>
								</li>
								<li class="parLi secondBallPip">
									<ul class="leftUl">
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
									</ul>
									<ul class="rightUl">
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
									</ul>
								</li>
								<li class="parLi thirdBallPip">
									<ul class="leftUl">
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
									</ul>
									<ul class="rightUl">
										<li></li>
										<li></li>
										<li></li>
										<li></li>
										<li></li>
									</ul>
								</li>
							</ul>
						</div>
						<div class="cutTime">
							<div>
								<span id="hourtxt">00:00:00</span>
								<span id="opening">開獎中...</span>
							</div>
						</div>
						<div class="tryKai">
							<span class="jzCheck" style="display: none;">開獎中,停止模擬</span>
							<span id="tryBtn"></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			var pW = $("html").width();
			var zoom = pW / 1125;
			var h = zoom * 780;
			var ty = navigator.userAgent.toLowerCase();
			if(ty.indexOf("trident") != -1 || ty.indexOf("firefox") != -1) {
				$("html").css({
					"transform-origin": "top left",
					"transform": "scale(" + zoom + ")"
				});
			} else {
				$("html").css({
					"zoom": zoom + ""
				});
			}
			try {
				$("iframe", window.parent.document).width(pW);
				$("iframe", window.parent.document).height(h);
			} catch(e) {

			}
		</script>
	</body>

</html>